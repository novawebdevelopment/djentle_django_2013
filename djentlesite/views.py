from django.views import generic
from django import forms

class ContactForm(forms.Form):
    subject = forms.CharField(max_length=100)
    message = forms.CharField()
    sender = forms.EmailField()

class ContactView(generic.FormView):
    form_class = ContactForm
    template_name = 'contact.html'

    def get_success_url(self):
        return '/contactconfirm' 

    def form_valid(self, form):
        subject = form.data['subject']
        message = form.data['message']
        sender = form.data['sender']
        print 'Subject: %s\nMessage: %s\nSender: %s' % (subject, message, sender)
        return super(ContactView, self).form_valid(form)
