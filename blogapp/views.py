from django.views import generic
from django import forms
from django.utils import timezone
from django.http import HttpResponseRedirect

from blogapp import models

class PostListView(generic.ListView):
    model = models.Post

class PostDetailView(generic.DetailView):
    model = models.Post

class PostForm(forms.ModelForm):
    class Meta:
        model = models.Post
        exclude = ['date_posted']

class PostEditView(generic.UpdateView):
    form_class = PostForm
    model = models.Post

    def get_success_url(self):
        return '/blogapp/posts/%s' % self.object.id

class PostAddView(generic.CreateView):
    form_class = PostForm
    model = models.Post

    def get_success_url(self):
        return '/blogapp/posts/%s' % self.object.id

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.date_posted = timezone.now()
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

class PostDeleteView(generic.DeleteView):
    model = models.Post
    success_url = '/blogapp/posts'

