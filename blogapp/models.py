from django.db import models

class Post(models.Model):
    author = models.CharField(max_length=50)
    title = models.CharField(max_length=140)
    content = models.CharField(max_length=2000)
    date_posted = models.DateTimeField('date posted')
